package models

final case class TodoItem(id: Int, title: String, completed: Boolean)
final case class NewTodoItem(title: String)
