package controllers

import play.api.mvc.ControllerComponents
import play.api.mvc.BaseController
import javax.inject.Singleton
import javax.inject.Inject

@Singleton
class UserController @Inject()(
  val controllerComponents: ControllerComponents
) extends BaseController {

}
