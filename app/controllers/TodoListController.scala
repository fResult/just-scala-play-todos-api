package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import scala.collection.mutable.ListBuffer
import models.TodoItem
import play.api.libs.json.Json
import models.NewTodoItem

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class TodoListController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  implicit val todoItemJsonFormat = Json.format[TodoItem]
  implicit val newTodoItemJsonFormat = Json.format[NewTodoItem]

  private val todoList = new ListBuffer[TodoItem]()
  todoList += TodoItem(1, "Eat Dogs", false)
  todoList += TodoItem(2, "Eat Cats", false)

  def all(): Action[AnyContent] = Action {
    if (todoList.isEmpty) {
      NoContent
    } else {
      Ok(Json.toJson(todoList))
    }
  }

  def byID(todoID: Int) = Action {
    val foundTodo = todoList.find(_.id == todoID)

    foundTodo match {
      case Some(todo) => Ok(Json.toJson(foundTodo))
      case None => NoContent
    }
  }

  def create() = Action { implicit request =>
    val jsonObject = request.body.asJson
    val newTodo: Option[NewTodoItem] = jsonObject.flatMap(Json.fromJson[NewTodoItem](_).asOpt)
    print(newTodo)

    newTodo match {
      case Some(todo) =>
        val nextID = todoList.map(_.id).max + 1
        val toBeAdded = TodoItem(nextID, todo.title, false)
        todoList += toBeAdded
        Created(Json.toJson(toBeAdded))
      case None => BadRequest
    }
  }

  def completeTodo(todoID: Int) = Action {
    val targetTodo = todoList.find(_.id == todoID)

    targetTodo match {
      case Some(todo) =>
        val completedTodo = todo.copy(completed = true)
        todoList.dropWhileInPlace(_.id == todoID)
        todoList += completedTodo
        Accepted(Json.toJson(completedTodo))
      case None => NotFound
    }
  }
}
